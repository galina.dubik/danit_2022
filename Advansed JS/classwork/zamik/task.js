// Напишите функцию, которая принимает в качестве параметра строку, а затем возвращает функцию, которая будет выводить эту строку в консоль. Используйте замыкание.

function stringFunction (string) {
    return function () {
        return string
    }
}
// console.log(stringFunction('Hello'));
let clg = stringFunction('Hello World!');
console.log(clg());