//
// напишите метод caclAverageScore, который будет считать среднюю оценку внутри объекта tabel.
// Оценка – это свойство объекта, имеющее тип «строка», значение которого представляет собой либо целое число (типа Number), либо целое число в виде строки.
//

const tabel = {
    biology: 11,
    phisic: 8,
    history: 12,
    mathematic: 6,
    sport: "6",
    name: "Генос",
    100: 20,
    200: 450,
    "last name": "Кибердемон",
};


function calcAverageScore() {
    const biology = Number(this.biology);
    const physic = Number(this.physic);
    const history = Number(this.history);
    const mathematic = Number(this.mathematic);
    const sport = Number(this.sport);
  
    return (biology + physic + history + mathematic + sport) / 5;
  }
  
  table.calcAverageScore = calcAverageScore;
  
  console.log(table.calcAverageScore());
  
  