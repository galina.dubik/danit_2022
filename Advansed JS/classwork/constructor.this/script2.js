// У вас есть массив оценок scoreArr.
// Выведите в консоль минимальную и максимальную оценки.
let scoreArr = [ 8, 6, 7, 11, 4, 12, 9, 10 ];

function minMax (array) {
    const min = Math.min.apply(null, array);
    const max = Math.max.apply(null, array);
    return (min, max)
}
console.log(minMax(scoreArr));
console.log({min, max} = minMax(scoreArr));