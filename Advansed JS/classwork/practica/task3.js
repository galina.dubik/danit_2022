class Product {
    constructor (id, name, price) {
        this._id = id;
        this._name = name;
        this._price = price;
    }

    makeDiscount (discount) {
        this._price = this._price * discount;
    }

    print () {
        console.log(`${this._id} \n ${this._name} \n ${this._price}`);
    }

    static compare (product1, product2) {
        return product1._price  > product2._price ? 'First product > price' : 'Second product > price';
    }

    get price () {
        return this._price;
    }
}

let fruit = new Product ("8888", "lime", 99);
let water = new Product ("4444", "Morshinska", 18);
console.log(Product.compare(fruit, water));

class Book extends Product {
    constructor (id, name, price, autors, pubDate) {
        super (id, name, price);
        this._autors = autors;
        this._pubDate = pubDate;
    }
    print () {
        super.print();
        console.log(`${this._autors} \n ${this._pubDate}`);
    }
}
const kolobok = new Book ("123", "Kolobok", 50, "Evgeniy", 2023);
kolobok.print();
  