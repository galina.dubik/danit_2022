// function ProductGeneration (brand) {
//     this.brand = brand;
//     this.model = model;
//     this.versio = version;
//     this.year = year;

// }
// ProductGeneration.prototype.powerOn = function () {
//     console.log(`${this.brand} ${this.model} is starting`);
// };
// ProductGeneration.prototype.powerOff =  function() {
//     console.log(`${this.brand} ${this.model} is shutting down`);
// };

// let notebook = new ProductGeneration( "Asus", "Zenbook", "112314", "2016")
// notebook.powerOn();
// notebook.powerOff();

function Product(brand, model, version, year) {
    this._brand = brand;
    this._model = model;
    this._version = version;
    this._year = year;
  }
  
  Object.defineProperty(Product.prototype, "brand", {
    get: function () {
      return this._brand;
    },
  });
  
  Object.defineProperty(Product.prototype, "model", {
    get: function () {
      return this._model;
    },
  });
  
  Object.defineProperty(Product.prototype, "version", {
    get: function () {
      return this._version;
    },
  });
  
  Object.defineProperty(Product.prototype, "year", {
    get: function () {
      return this._year;
    },
    set: function (value) {
      this._year = value;
    },
  });
  
  Product.prototype.powerOn = function () {
    console.log(`${this.brand} ${this.model} turning on`);
  };
  
  Product.prototype.powerOff = function () {
    console.log(`${this.brand} ${this.model} turning off`);
  };
  
  let notebook = new Product("Asus", "Tuf Gaming", "3.0.1", 2021);
  notebook.powerOn();
  notebook.powerOff();
  
  notebook.processor = "Intel Core i5-2670U";
  notebook.work = function () {
    console.log(`${this.brand} ${this.model} working`);
  };
  
  console.log(notebook.processor);
  notebook.work();
  