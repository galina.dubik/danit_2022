/**
 *
 * Создайте 3 объекта, каждый со своими методами и свойствами:
 *    товар - Ноутбуки,
 *    товар - Телефоны,
 *    товар - Стиральные машинки.
 *    Повторяющиеся методы и свойства вынесите в прототип.
 *
 * */

 let product = {
    powerOn: function () {
        console.log(`${this.brand} ${this.model} is starting`);
    },
    powerOff: function () {
        console.log(`${this.brand} ${this.model} is shutting down`);
    },
    get brand () {
        return this._brand
    },
    get model () {
        return this._model
    },
    get versio () {
        return this._versio
    },
    get year () {
        return this._year
    },
    set year (value) {
        this._year = value;
    },
};
let nootbook = {
    _brand: "Asus",
    _model: "Zenbook",
    _versio: 112314,
    _year: 2016,
    __proto__:  product
    
}
let phone = {
    _brand: "Apple",
    _model: "Iphone",
    _versio: 14,
    _year: 2022,
    __proto__:  product
    
}
let washingMachine = {
    _brand: "Bosch",
    _model: "Wan2428",
    _versio: 8,
    _year: 2020,
    __proto__:  product
  
}
console.log(nootbook, phone, washingMachine);
nootbook.powerOn();
washingMachine.powerOff();