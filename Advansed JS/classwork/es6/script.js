// Задание: 
    // 1. Напишите класс "Input", который создает объект, описывающий однострочное 
    // поле ввода.
    // У объекта будут такие свойства:
    // - тип поля ввода;
    // - классы;
    // - id;
    // и метод:
    // - render, возвращающий DOM-элемент, созданный на основе свойств объекта.
    
    // Вынесите этот класс в отдельный модуль, импортируйте еевосновной файл main.js,
    // создайте однострочное поле ввода и выведите его на экран.

    // <input type="text" class="form-control size-lg" id="user-name"> 

    import Input from "./modules/input.js";
    import Form from "./modules/form.js";

    const userNameInput = new Input({
      type: "text",
      classes: ["form-control", "size-lg"],
      id: "user-name",
    });
    const userEmailInput = new Input({
      type: "email",
      classes: ["form-control", "size-lg"],
      id: "user-email",
    });
    const userPasswordInput = new Input({
      type: "password",
      classes: ["form-control", "size-lg"],
      id: "user-password",
    });
    const registrForm = new Form ({
      classes: ["element form-elem"],
      id: "form",
      inputs: [userNameInput, userEmailInput, userPasswordInput]
    })
    
    document.body.appendChild(userNameInput.render());
    document.body.appendChild(userEmailInput.render());
    document.body.appendChild(userPasswordInput.render());
    document.body.appendChild(registrForm.render());
    