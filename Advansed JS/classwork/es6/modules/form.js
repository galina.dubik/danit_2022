// Создать класс Form который принимает на вход:
// классы
// id,
// Инпуты которые она будет содержать

// Добавить метод render возвращающий DOM-элемент, созданный на основе свойств объекта.

class Form {
    constructor({classes, id, inputs}) {
        this.classes = classes;
        this.id = id;
        this.inputs = inputs;
    }
    render() {
        const form = document.createElement("form");
        form.classList.add(...this.classes);
        form.append(...this.inputs);
        form.id = this.id;
        return form
    }
}