/**
 * # Завдання
 * Реалізувати функціонал форми для створення нових користувачів
 *
 * ## Технічні вимоги
 * * АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 * * при натисканні на кнопку форми відправляти POST запит з введеною інформацією
 * * при успішному виконанню запиту показувати модальне вікно (alert), що користувач створений
 *
 * */

// const API = "https://ajax.test-danit.com/api-pages/jsonplaceholder.html";


let formInfo = document.getElementById("form");

formInfo.addEventListener('submit', function (event) {
    event.preventDefault();
    
    const firstName = event.target["first-name"].value;
    const lastName = event.target["last-name"].value;
    
    if (firstName.trim() === "" || lastName.trim() === "") {
        return;
    }
    fetch ("https://ajax.test-danit.com/api/json/users", {
        method: "POST",
        body: JSON.stringify({
            name: firstName,
            fullName: [firstName, lastName]
        }),
        headers: {
            'Content-type': 'application/json'
        }
    }).then((response) => {
        if (response.ok) {
            alert("Користувач створено");
        } else {
            alert("Помилка створення користувача")
        }
    })
})
    













// const form = document.querySelector('form');
// form.addEventListener('submit', (event) => {
//   event.preventDefault();
//   const firstName = document.querySelector('#first-name').value;
//   const lastName = document.querySelector('#last-name').value;
  
//   fetch('https://jsonplaceholder.typicode.com/users', {
//     method: 'POST',
//     body: JSON.stringify({
//       name: `${firstName} ${lastName}`,
//       username: `${firstName.toLowerCase()}.${lastName.toLowerCase()}`,
//           }),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8',
//     },
//   })
//   .then(response => {
//     if (response.ok) {
//       alert('Користувач створений!');
//       form.reset();
//     } else {
//       throw new Error('Помилка при створенні користувача');
//     }
//   })
//   .catch(error => {
//     console.log(error);
//   });
