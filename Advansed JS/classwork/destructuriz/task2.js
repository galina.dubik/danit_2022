const armodroms = ["Отравленный кинжал", "Золотой бог", "Гарганто", "Фламберг", "Рыцарь"];
const airships = ["Золар Ауперкаль", "Казнь", "Вечный голод", "Покровитель"];

const ships = [...armodroms, ...airships];
console.log(ships);

const ships1 = armodroms.concat(airships);
console.log(ships1);

const ships2 = airships.concat(armodroms);
console.table(ships2);

// У вас есть 2 массива -
// объедините их содержимое
// в один warMachine максимально элегантным способом: