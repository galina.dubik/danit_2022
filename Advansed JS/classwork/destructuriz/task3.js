// напишите функцию, которая создает объект.
// В качестве аргументов она принимает в себя имя, фамилию, и перечень
// строк формата “имяСвоства: значение“. Их может быть много.
// пример работы:


const user = createObject("Золар", "Аперкаль", "status: глава Юного клана Мескии", "wife: Иврейн", "hobbie: footbool");
console.log(user);

// user = {
//     name: "Золар",
//     lastName: "Аперкаль",
//     status: "глава Юного клана Мескии",
//     wife: "Иврейн"
// }

function createObject(name, lastName, ...options) {
    let obj = {name, lastName};
    options.forEach(option => {
        const [key, value] = option.split(": ", 2);
        obj[key] = value;
    });
    return obj;
}



// function СreateObject(name, surname, ...args) {
//     this.name = name;
//     this.surname = surname;
//     args.forEach((option) => {
//       const [key, value] = option.split(": ", 2);
//       this[key] = value;
//     });
//   }
//   const newUser = new СreateObject('Volodymyr', 'Cherniuk', 'wife: tudu', 'lalal: kuku');
//   console.log(newUser);