let user = {
    name: 'John',
    year: 30,
};
const {name, years: age, isAdmin = false} = user;

console.log({name, age, isAdmin});