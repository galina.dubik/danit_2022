function Hamster(name, age) {
    this.name = name;
    this.age = age;
    this.stomach = [];
  }
  
  Hamster.prototype.eat = function (food) {
    this.stomach.push(food);
  };
  
  const speedy = new Hamster("Speedy", 20);
  const lazy = new Hamster("Lazy", 20);
  
  console.log(speedy.name);
  console.log(lazy.name);
  
  speedy.eat("pizza");
  console.log("Speedy stomach", speedy.stomach);
  
  console.log("Lazy stomach", lazy.stomach);