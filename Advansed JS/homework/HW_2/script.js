const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const divId = document.getElementById("root");
let ulEl = document.createElement("ul");
divId.prepend(ulEl);

books.forEach(book => {
  const li = document.createElement('li')
  li.textContent = `${book.author}; ${book.name}; ${book.price}`
try {
    if (book.author === undefined) {
      li.textContent = ''
      throw new Error(`Відсутій автор ${books.author}`)
    }
    if (!book.name) {
      li.textContent = ''
      throw new Error(`Відсутня назва ${books.name}`)
    }
    if (!book.price) {
      li.textContent = ''
      throw new Error(`Відсутій прайс ${books.price}`)
    }
    
  } catch (error) {
    console.log(error);
    return
  }
  divId.appendChild(li)
})




// function string(obj) {

//   // str1 = `<div>${obj.author}</div>`;
//   // str2 = `<div>${obj.name}</div>`;
//   // str3 = `<div>${obj.price}</div>`;

//   str1 = `<div>${obj.author.toString()}</div>`;
//   str2 = `<div>${obj.name.toString()}</div>`;
//   str3 = `<div>${obj.price.toString()}</div>`;
//   return "<br />" + str1 + str2 + str3;
// }
// const list = books.map((obj) => {
//   try {
//     return `<li>${string(obj)}</li>`;
//   } catch (err) {
//     console.log("error", err);
//     return "";
//   }
// });
// ulEl.innerHTML = list.join(""); 
//зєднує масив в один рядок
























// let list = books.map((obj) => {
//   const li = document.createElement('li')
//   li.textContent = `${obj.author}; ${obj.name}; ${obj.price}`
//   let liInfo  = "";
//   for (const key in obj) {
    
//     if (Object.hasOwnProperty.call(obj, key)) {
//       const element = obj[key];
//       ulEl  = `${element}`;
//       console.log(ulEl);
//     }
//   }
//   // liInfo = `${obj.author}`
//   // console.log(liInfo);
//   // console.log(obj);

//   divId.appendChild(li)
// })

// ulEl.innerHTML = list.join(""); 
// // зєднує масив в один рядок












// const list = books.map(strings => `<li>${JSON.stringify(strings)}</li>`);

// const string = (obj) => {
//   let str = "";
//   for (let key in obj) {
//     str = `${str} ${key} : ${obj[key]}`;
//     console.log(str);
//   }
//   return str;
// };

// ulEl.innerHTML = list.join(""); 
//зєднує масив в один рядок