// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const url = "https://ajax.test-danit.com/api/swapi/films";






async function getAjax(url) {
  const response = await fetch(url);
  const result = await response.json();
  return result;
  // return await response.json()
}

getAjax("https://ajax.test-danit.com/api/swapi/films").then((result) => {
  console.log(result);
  result.forEach((objectFilm) => {
    const docBody = document.createElement("p")
    docBody.textContent = `${objectFilm.episodeId} ${objectFilm.name} ${objectFilm.openingCrawl}`
    document.body.appendChild(docBody);
    objectFilm.characters.forEach((objPerson) => {
      getAjax(objPerson).then((person) => {
        const personName = document.createElement("div");
        personName.textContent = `${person.name}`;
        docBody.appendChild(personName);
        // console.log(ar.push(person.name));
      });
    });
  });
  // console.log(docBody);
});




















// async function getAjax(url) {
//   const response = await fetch(url);
//   const result = await response.json();
//   // console.log(result);
//   return result;
// }
// async function main(url) {
//   const films = await getAjax(url);
//   let charUrls = []; 
//   console.log(films);
//     films.forEach((film) => {
//       // console.log(film.characters);
//       charUrls = [...charUrls, ...film.characters];
//     });

//     for (let i = 0; i < charUrls.length; i++) {
//       let char = await getAjax(charUrls[i]);
//       printChar(char.name);
//     }
// }

// function printChar(name) {
//   const docBody = document.createElement("div");
//   docBody.textContent = `${name}`;
//   document.body.appendChild(docBody);
// }
// main(url);













// async function getAjax(url) {
//   const response = await fetch(url);
//   const result = await response.json();
//   // console.log(result);
//   return result;
// }
// async function main(url) {
//   const films = await getAjax(url);
//   let charUrls = []; 
//   console.log(films);
//     films.forEach((film) => {
//       // console.log(film.characters);
//       charUrls = [...charUrls, ...film.characters];
//     });

//     for (let i = 0; i < charUrls.length; i++) {
//       let char = await getAjax(charUrls[i]);
//       printChar(char.name);
//     }
// }

// function printChar(name) {
//   const docBody = document.createElement("div");
//   docBody.textContent = `${name}`;
//   document.body.appendChild(docBody);
// }
// main(url);











// async function getAjax(url) {
//     const response = await fetch(url);
//     const result = await response.json();
//     return result;
//   }
//   async function main(url) {
//     const films = await getAjax(url);
//     for (let i = 0; i < films.length; i++) {
//       for (let j = 0; j < films[i].characters.length; j++) {
//         let char = await getAjax(films[i].characters[j]);
//         printChar(char.name);
//       }
//   }
// }

//   function printChar(name) {
//     const docBody = document.createElement("div");
//     docBody.textContent = `${name}`;
//     document.body.appendChild(docBody);
//   }
//   main(url);




















// function fetchPost() {
//   return fetch(url)
//   .then((response) => response.json())
// }

// const list = document.getElementById("list")
// window.onload = function () {
//   // console.log(list);
//   fetchPost()
//   .then((listo) => {
//     for (const posts of listo) {
//       const li = document.createElement("li")
//       li.textContent = `${posts.name}`
//       document.body.appendChild(li)
//       console.log(posts.name);
//     }
//   })
//   .catch((error) => console.error(error))
// }
