// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.



main("https://ajax.test-danit.com/api/swapi/films")

async function main(url) {
    const films = await getAjax(url);
    for (let i = 0; i < films.length; i++) {
        let filmHtml = getFilmHtml(films[i])
        for (let j = 0; j < films[i].characters.length; j++) {
            let char = await getAjax(films[i].characters[j]);
            getCharacterHtml(filmHtml, char);
        }
    }
}
async function getAjax(url) {
     const response = await fetch(url);
     const result = await response.json();
     return result;
}

function getFilmHtml (film) {
    const el = document.createElement('section')
    el.innerHTML=`
        <div class="film">
            <div class="filmName"> <h2> <i>Назва: </i>${film.name || ''}</h2></div>
            <div class="shortInfo"> 
                <div><p><i>Епізод: </i> ${film.episodeId || ''}</p></div>
                <div><p><i>Дата: </i> ${film.releaseDate || ''}</p></div>
            </div>
            <div class="addInfo">
                <p>${film.openingCrawl || ''}</p>
                <ul class="charactes"></ul>
            </div>
        </div>`
    document.querySelector('.films').appendChild(el);
    return el
}
function getCharacterHtml (filmHtml , char) {
    const el = document.createElement('li')
    el.innerHTML=`
        <div class="character">
            <div><h4><i>Ім'я: </i>${char.name || ''}</h4></div>
            <div> 
                <div><p><i>Пол: </i>${char.gender || ''}</p></div>
                <div><p><i>Народився: </i>${char.birthYear || ''}</p></div>
            </div>
        </div>`
    filmHtml.querySelector('.charactes').appendChild(el);
    return el
}
















// async function getAjax(url) {
//     const response = await fetch(url);
//     const result = await response.json();
//     return result;
//     // return await response.json()
//   }

//   getAjax("https://ajax.test-danit.com/api/swapi/films").then((result) => {
//     console.log(result);
//     result.forEach((objectFilm) => {
//       objectFilm.characters.forEach((objPerson) => {
//         getAjax(objPerson).then((person) => {
//           const docBody = document.createElement("div");
//           docBody.textContent = `${person.name}`;
//           document.body.appendChild(docBody);
//           // console.log(ar.push(person.name));
//         });
//       });
//     });
//     // console.log(docBody);
//   });
