import {              config          } from "./js/config.js"         ; 
import {              COMPOSE         } from "./js/function/index.js" ; 
import { get        , post            } from "./js/http/http.js"      ; 
import { getFilmHtml, getCharacterHtml} from "./js/templates/films.js";

const charHtml = film   => char   =>  {  const printHtml  = getCharacterHtml(film); char?.map( char => printHtml(char) ) }
const getChar  = async characters =>     await Promise.all( characters?.map ( async  char => await get(char) ) || [] ) 

const prChar   =  films  =>  films .map( async film  => { const charCurry = charHtml(film) ; COMPOSE( getChar , charCurry )(film.characters) } );
const prFilms  =  films  =>  films?.map( film  => { return {...film, html:getFilmHtml(film) } } )  || []

const url      =  ()     => config.api.url

COMPOSE( url, get, prFilms, prChar ) (' S T A R T ')