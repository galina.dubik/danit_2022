
export const getFilmHtml =(film)=> {
    const el = document.createElement('section')
    el.innerHTML=`
        <div class="film">
            <div class="filmName"> <h2> <i>Назва: </i>${film.name || ''}</h2></div>
            <div class="shortInfo"> 
                <div><p><i>Епізод: </i> ${film.episodeId || ''}</p></div>
                <div><p><i>Дата: </i> ${film.releaseDate || ''}</p></div>
            </div>
            <div class="addInfo">
                <p>${film.openingCrawl || ''}</p>
                <ul class="charactes"></ul>
            </div>
        </div>`
    document.querySelector('.films').appendChild(el);
    return el
}

export const getCharacterHtml = film => char => {
    const el = document.createElement('li')
    el.innerHTML=`
        <div class="character">
            <div><h4><i>Ім'я: </i>${char.name || ''}</h4></div>
            <div> 
                <div><p><i>Пол: </i>${char.gender || ''}</p></div>
                <div><p><i>Народився: </i>${char.birthYear || ''}</p></div>
            </div>
        </div>`
    film.html.querySelector('.charactes').appendChild(el);
    return el
}