class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name () {
        return this._name; 
    }
    get age () {
        return this._age; 
    }
    get salary () {
        return this._salary; 
    }
    set name (value) {
        this._name = value;;
    }
    set age (value) {
        this._age = value;
    }
    set salary (value) {
        this._salary = value;
    }
}


class Programer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary * 3;
    }
}

const accountantHalina = new Programer("Halina", 31, 18000, "ua");
console.log(accountantHalina);
const accountantInna = new Programer("Inna", 38, 16000, "ua");
console.log(accountantInna);
