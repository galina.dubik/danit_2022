// task1

// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.

// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const [...fullname] = clients1;
const [...fullname2] = clients2;

const concatAr = new Set([...fullname, ...fullname2])
console.log(concatAr);
// console.log([...fullname, ...fullname2]);



// const [gilbert, salvatore, pierce, sommers, forbes, donovan, bennett] = clients1;

// const [pierce2, saltzman, salvatore2, michaelson] = clients2;

// const concatArray = new Set ([gilbert, salvatore, pierce, sommers, forbes, donovan, bennett, pierce2, saltzman, salvatore2, michaelson]);
// console.log(concatArray);


