// task3

// У нас є об'єкт' user:

const user1 = {
  name: "John",
  years: 30
};

// Напишіть деструктуруюче присвоєння, яке:

// властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
// Виведіть змінні на екран.

const {name = name, years = age, isAdmin = false} = user1;
// const {name, years, isAdmin = false} = user1;

console.log({name, years, isAdmin});
document.body.prepend(`${name}, ${years}, ${isAdmin}`);

// years:age перейменування