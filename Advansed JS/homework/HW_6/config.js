
        // const compose2  = (f, g) => async (...args) => f(await g(...args))
        // export  const COMPOSE   = (...fns) => fns.reduceRight(compose2)
        
        const request = (url, params = {}, method = "GET", format = null) => {
                let options = {
                  method,
                };
                if ("GET" === method) {
                  url += "?" + new URLSearchParams(params).toString();
                } else {
                  options.body = JSON.stringify(params);
                }
                if (format) {
                  return fetch(url, options)
                    .then((response) => response.text())
                    .catch((err) => {
                      console.log("err");
                    });
                } else {
                  return fetch(url, options)
                    .then((response) => response.json())
                    .catch((err) => {
                      console.log("err");
                    });
                }
              };
              const getTxt = (url, params) => request(url, params, "GET", "text");
              const get = (url, params) => request(url, params, "GET");
              const post = (url, params) => request(url, params, "POST");
              
              async function main(url) {
                const api = "http://ip-api.com/json/";
                const ip = await getTxt(url);
                console.log("ip", ip);
                const apiUrl = api + ip;
                const address = await get(apiUrl);
                console.log("address", address);
                infoHtml(address);
              }
              
              const btn = document.createElement("button");
              btn.innerHTML = "Знайти по IP";
              document.body.prepend(btn);
              
              btn.addEventListener("click", function (event) {
                event.preventDefault();
                const url = "https://api.ipify.org/?format=json";
                main(url);
              });
              
              function infoHtml(address) {
                const el = document.createElement("section");
                el.innerHTML = `
                      <p>Континент: ${address?.timezone || ""}</p>
                      <p>Країна: ${address?.country || ""}</p>
                      <p>Регіон: ${address?.region || ""}</p>
                      <p>Місто: ${address?.city || ""}</p>
                      <p>Район: ${address?.regionName || ""}</p>
                      `;
                document.body.appendChild(el);
                return el;
              }
            
              