// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:

// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.createElement("button");
btn.innerHTML = "Знайти по IP";
document.body.prepend(btn);

btn.addEventListener("click", function (event) {
  event.preventDefault();

  async function getIP() {
    const response = await fetch("https://api.ipify.org/?format=json");
    const result = await response.json();
    return result.ip;
  }
  async function getIPInfo(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}`);
    const data = await response.json();
    return data;
  }
  getIP().then((ip) => getIPInfo(ip).then(data => infoHtml(data)));
  

  function infoHtml({timezone, country, region, city, regionName}) {
    const el = document.createElement("section");
    el.innerHTML = `
            <p>Континент: ${timezone}</p>
            <p>Країна: ${country}</p>
            <p>Регіон: ${region}</p>
            <p>Місто: ${city}</p>
           <p>Район: ${regionName}</p>
            `;
    document.body.appendChild(el);
    return el;
  }
  // infoHtml();
});

