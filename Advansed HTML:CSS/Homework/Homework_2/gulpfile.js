const gulp = require("gulp");
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));
const minifyjs = require("gulp-js-minify");
const uglify = require("gulp-uglify");
const cleanCSS = require("gulp-clean-css");
const imagemin = require("gulp-imagemin");
const autoprefixer = require("gulp-autoprefixer");
// const clean = require('gulp-clean');

const path = {
  src: {
    scss: './src/scss/**/*.scss',
    js: "./src/js/*.js",
    img: "./src/img/**/*",
  },
  dist: {
    self: "dist/",
    css: "./dist/css/",
    js: "./dist/js/",
    img: "./dist/img/",
  },
};

const buildScss = () =>
  gulp
    .src(path.src.scss)
    .pipe(sass().on("error", sass.logError))
    .pipe(concat("styles.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(gulp.dest(path.dist.css))
    .pipe(browserSync.stream());

const buildJs = () =>
  gulp
    .src(path.src.js)
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(minifyjs())
    .pipe(gulp.dest(path.dist.js))
    .pipe(browserSync.stream());

const buildImg = () =>
  gulp.src(path.src.img).pipe(imagemin()).pipe(gulp.dest(path.dist.img));

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch(path.src.scss, buildScss).on("change", browserSync.reload);
  gulp.watch(path.src.js, buildJs).on("change", browserSync.reload);
  gulp.watch(path.src.img, buildImg).on("change", browserSync.reload);
};

// const cleanDist = () => (
//     gulp.src('dist', {read: false})
//         .pipe(clean())
// );

// gulp.task("dev", gulp.series(cleanDist));
gulp.task("build", gulp.parallel(buildImg, buildScss, buildJs, watcher));









// const gulp = require("gulp"),
//     concat = require('gulp-concat')

// gulp.task('css', function (done) {
//     gulp.src('./src/css/**/*.css')
//     .pipe(concat('main.css'))
//     .pipe(gulp.dest('./dist/css/'))
//     .on('end', done);
// })
// gulp.task('js', function (done) {
//     gulp.src('./src/js/**/*.js')
//     .pipe(concat('main.js'))
//     .pipe(gulp.dest('./dist/js/'))
//     .on('end', done);
// })
// gulp.task('default', gulp.series('css', 'js'))