const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const uglify = require("gulp-uglify")
const sass = require("gulp-sass")(require("sass"))
const concat = require("gulp-concat")

/*
  -- TOP LEVEL FUNCTIONS --
  gulp.task - Define tasks 
  gulp.src - Point to files to use
  gulp.dest - Points to folder to output
  gulp.watch - Watch files and folders for changes
*/

gulp.task("message", function (done) {
  console.log("Hello  World!");
  done();
});

// npm i = npm install
// npm un = npm un install
// npm i --save-dev = npm i -D




gulp.task("html", function (done) {
  gulp.src("./src/index.html").pipe(gulp.dest("./build")).on("end", done);
});

gulp.task("image", function (done) {
  gulp
  .src("./src/image/*")
  .pipe(imagemin())
  .pipe(gulp.dest("./build/image"))
  .on("end", done);
});


// minify js

gulp.task("js", function (done) {
  gulp
  .src("./src/js/*.js")
  .pipe(uglify())
  .pipe(gulp.dest("./build/js"))
  .on("end", done);
});



gulp.task("default", gulp.series("message", "html", gulp.parallel("image", "js")));

gulp.task("sass", function (done) {
  gulp
  .src("src/sass/**/*.scss")
  .pipe(sass().on("error", sass.logError))
  .pipe(gulp.dest("build/css"))
  .on("end", done)
})