import React from "react";

class Header extends React.Component {

    // пишуться методи

  render() {  // const пишемо  між render та return

    const daysArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',];
    const monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // отримати поточний час на момент запису коду
    const newDate = new Date ()   // від нього будемо вираховувати поточну дату (день, дні тижня, місяць)
    const day = newDate.getDate()  // від поточного часу отримати день
    //const days = newDate.getDay()  // від поточного часу отримати день тижня (отримаємо індекс день тижня)
    //const month = newDate.getMonth()  //від поточного часу отримати місяць (отримаємо індекс місяця)

    // звязати з масивами - звернутися до масиву по індексу
    const days = daysArr[newDate.getDay()] // по індексу отримати назву з масиву
    const months = monthsArr[newDate.getMonth()]
    return (

      <div className="header-wrapper">
        <div className="header">
          <div className="current-date">
            <p className="day">{days}</p>
            <p className="data">{day} {months}</p>
          </div>
        </div>
      </div>
    );
  }
}
export default Header;