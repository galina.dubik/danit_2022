import React from "react";
import "./modal.scss";
import { ReactComponent as CloseIcon } from "./img/closeIcon.svg";

class Modal extends React.Component {
  render() {
    const { header, text, actions, onClick, closeButton, bcgColor } = this.props;
    return (
        <div className="modal__background" onClick={onClick} >
          <div className="modal" onClick={elem => {elem.stopPropagation()}} style={{ backgroundColor: bcgColor }}>
            <div className="modal__header">
              <h2 className="modal__title">{header}</h2>
              {closeButton && <CloseIcon onClick={onClick} />}
            </div>
            <div className="modal__content">
              <p className="content-text">{text}</p>
            </div>
            <div className="modal__btn">{actions}</div>
          </div>
        </div>
    );
  }
}

export default Modal;
