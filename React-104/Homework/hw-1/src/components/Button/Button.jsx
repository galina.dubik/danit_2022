import React from "react";
import "./button.scss"

class Button extends React.Component {
  render() {
    const { bcgColor, text, onClick } = this.props;
    return (
      <button className="btn" style={{ backgroundColor: bcgColor }} onClick={onClick}>
        {text}
      </button>
    );
  }
}

export default Button;
