// Створити новий React додаток з двома модальними вікнами.

// Технічні вимоги:
//// Створити програму за допомогою create-react-app.
//// Створити на головній сторінці 2 кнопки з текстом Open first modal та Open second modal.
//// По кліку на кожну з кнопок має відкриватись відповідне модальне вікно.
//// Створити компонент Button який повинен мати такі властивості, що передаються з батьківського компонента:
//// Колір фону (властивість backgroundColor)
//// Текст (властивість text)
//// Функція при натисканні (властивість onClick)
//// Створити компонент Modal який повинен мати такі властивості, що передаються з батьківського компонента:
//// Текст заголовка модального вікна (властивість header)
//// Чи повинен бути хрестик для закриття вікна праворуч вгорі (boolean властивість closeButton, значення true/false)
//// Основний текст модального вікна, який буде показаний у його центральній частині (властивість text)
//// Кнопки, що знаходяться у нижній частині модального вікна, передані у вигляді коду у форматі JSX (властивість actions)
//// При відкритому модальному вікні частина сторінки, що залишилася, повинна бути затемнена за допомогою темного напівпрозорого фону.
//// Модальне вікно має закриватися при натисканні області зовні його контенту.
//// Стилізувати кнопки та модальні вікна за допомогою SCSS
//// Кнопки мають бути різних кольорів
//// Модальні вікна мають містити різний текст.
//// Дизайн модального вікна дається в PSD файлі.
//// Одне модальне вікно зробити як у дизайні. Для другого потрібно використовувати інший текст та інші кнопки (виберіть будь-які).
//// Усі компоненти мають бути створені у вигляді ES6 класів.

import React from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
  btnFirstModal = "Open first modal";
  btnSecondModal = "Open second modal";

  state = {
    isFirstModal: false,
    isSecondModal: false
  };

  openFirstModal = () => {
    this.setState({
      isFirstModal: true
    })};

  openSecondModal = () => {
    this.setState({
      isSecondModal: true
    })};

  closeModal = () => {
    this.setState({
      isFirstModal: false,
      isSecondModal: false
    })
  }
  // openFirstModal = () => {
  //   this.setState((prevState) => ({
  //     isFirstModal: !prevState.isFirstModal,
  //   }));
  // };
  // openSecondModal = () => {
  //   this.setState((prevState) => ({
  //     isModal: !prevState.isModal,
  //   }));
  // };


  render() {
    const { isFirstModal, isSecondModal } = this.state;
    return (
      <>
        <Button
          bcgColor="blue"
          text={this.btnFirstModal}
          onClick={this.openFirstModal}
        />
        <Button
          bcgColor={"yellow"}
          text={this.btnSecondModal}
          onClick={this.openSecondModal}
        />
        {isFirstModal && (
          <Modal
            className="modal1"
            bcgColor="blue"
            closeButton={true}
            onClick={this.closeModal}
            header="Do you want to delete this file?"
            text="Onse you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
            actions={
              <>
                <button className="modal__btn--ok" onClick={this.closeModal}>Ok</button>
                <button className="modal__btn--cancel" onClick={this.closeModal}>Cansel</button>
              </>
            }
          ></Modal>
        )}
        {isSecondModal && ( 
          <Modal
            className="modal2"
            bcgColor="yellow"
            closeButton={true}
            onClick={this.closeModal}
            header="Do you want to delete this file?"
            text="Are you sure you want to delete it?"
            actions={
              <>
                <button className="modal__btn--ok" onClick={this.closeModal}>Ok</button>
                <button className="modal__btn--cancel" onClick={this.closeModal}>Cansel</button>
              </>
            }
          ></Modal>
         )}
      </>
    );
  }
}

export default App;

