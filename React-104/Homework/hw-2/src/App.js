// Створити сторінку інтернет-магазину.

// Для цього необхідно доповнити проект, створений у попередньому домашньому завданні homework1.

// Технічні вимоги:
// Створити масив із колекцією товарів інтернет-магазину.
// Один товар повинен містити такі дані
// Назва
// Ціна
// Шлях до картинки (url в інтернеті або шлях до файлу в папці public)
// Артикул (будь-які цифри)
// Колір – Всього товарів має бути не менше 10. Тематика магазину – будь-яка.
// Покласти масив у JSON файл, який зберігатиметься в папці public вашого проекту.
// За допомогою AJAX запиту отримати дані з масиву товарів, записати в локальний state компонента головної сторінки.
// Вивести на сторінку перелік товарів. Дизайн можна взяти з PSD файлу з секції LATEST ARRIVALS IN MUSICA або будь-який свій. Дизайн може бути будь-який, але він має бути.
// Картка товару та список товарів обов'язково повинні бути реалізовані як окремі компоненти.
// При натисканні на кнопку Add to cart має з'являтися модальне вікно з підтвердженням додавання товару в кошик (використовуйте відповідний компонент з homework1.
// Також на кожній картці товару має бути іконка зірочки, яка дозволить додати товар до обраного. Якщо товар обраний – зірочка має бути зафарбована будь-яким кольором.
// При додаванні товару в кошик або у вибране, зберігати відповідну зміну в localStorage.
// У шапці сайту показувати іконки кошика та обраного, поряд з якими має бути зазначена кількість товарів, які були додані до кошика чи обране.
// Проект можна стилізувати за допомогою JSS чи SCSS.
// Усі компоненти мають бути створені у вигляді ES6 класів.
// Властивості, які передаються у компоненти, повинні бути перевірені за допомогою propTypes.
// Для всіх властивостей, які не є обов'язковими, необхідно вказати defaultProps.
// На даний момент у додатку має бути лише одна сторінка – головна сторінка зі списком товарів.


//sfc
// const  = () => {
//     return (  );
// }
 
// export default ;



import './App.css';
import './App.scss';
import React, {useEffect, useState} from 'react'; 
import List from './components/List/List';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import IconBasket from './components/Basket/Basket';
import IconSelected from './components/Selected/Selected';


function App() {
  const [state, setState] = useState([])
  const [modal, setModal] = useState(null)
  const [stateCard, setStateCard] = useState(JSON.parse(localStorage.getItem("stateCard"))|| [])
  const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite"))|| [])

  useEffect(() => {
    fetch('./array.json')
    .then((res) => res.json())
    .then((result) => {
      console.log(result);       // За допомогою AJAX запиту отримати дані з масиву товарів, записати в локальний state компонента головної сторінки.

      setState(result)
      // setState({state:result})

    })
 
  }, [])
  const openClick = (id) => {
    setModal( id );
  };
  const closeClick = () => {
    setModal(null );
  };
  const addToCard = () => {
    setStateCard((elem) => {
      return  [...elem, modal]
    
    })
  }
  const toggleFavorite = (id) => {
    setFavorite((prev)=>{
      let newArr 
      if(prev.includes(id)){ //Метод includes() определяет, содержит ли массив определённый элемент, возвращая в зависимости от этого true или false.
        newArr = prev.filter((value) =>  value !== id)
      } else {
        newArr = [...prev, id]
      }
      return newArr
    })
    
  }

  // При додаванні товару в кошик або у вибране, зберігати відповідну зміну в localStorage.
  useEffect(() => {
    localStorage.setItem("stateCard", JSON.stringify(stateCard))
  }, [stateCard])

  useEffect(() => {
    localStorage.setItem("favorite", JSON.stringify(favorite))
  }, [favorite])


  return(
    <div className="App">
      <div className="section-icon">
        <div> {/* У шапці сайту показувати іконки кошика та обраного, поряд з якими має бути зазначена кількість товарів, які були додані до кошика чи обране. */}
          <IconBasket className="basket"/>
          <p className="basket-text">{stateCard.length}</p>
        </div>
        <div>
          <IconSelected className="selected" />
          <p className="selected-text">{favorite.length}</p>
        </div>
      </div>
      <List arrayState={state} openClick={openClick} toggleFavorite={toggleFavorite}  favorite={favorite}></List>
        {modal && <Modal header={"Add product to cart"} text={"Add product"} closeButton={true} onClick={closeClick} actions={<div className="modal__style">
        <Button text={"Ok"} onClick={() => {addToCard(); closeClick()} }/>
        <Button onClick={closeClick} text={"Cancel"}/>
        </div>}/>}
    </div>
  ) // Вивести на сторінку перелік товарів. Дизайн можна взяти з PSD файлу з секції LATEST ARRIVALS IN MUSICA або будь-який свій. Дизайн може бути будь-який, але він має бути.
  // При натисканні на кнопку Add to cart має з'являтися модальне вікно з підтвердженням додавання товару в кошик (використовуйте відповідний компонент з homework1.

}

export default App;


