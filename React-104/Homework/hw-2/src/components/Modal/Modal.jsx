import "./modal.scss";
import { ReactComponent as CloseIcon } from "./../Modal/img/closeIcon.svg"
import PropTypes  from "prop-types"

function Modal ({header, text, actions, onClick, closeButton, bcgColor}){
    return (
        <div className="modal__background" onClick={onClick} >
          <div className="modal" onClick={elem => {elem.stopPropagation()}} style={{ backgroundColor: bcgColor }}>
            <div className="modal__header">
              <h2 className="modal__title">{header}</h2>
              {closeButton && <CloseIcon onClick={onClick} />}
            </div>
            <div className="modal__content">
              <p className="content-text">{text}</p>
            </div>
            <div className="modal__btn">{actions}</div>
          </div>
        </div>
    )
}

Modal.defaultProps = {
	onClick: () => {},
	closeButton: () => {},
  
}
Modal.propTypes = {
	header: PropTypes.string,
	onClick: PropTypes.func,
	text: PropTypes.string,
  bcgColor: PropTypes.string
}

export default Modal;