import React from "react";
import Button from "./../Button/Button"
import "./card.scss";
import {ReactComponent as ReactImg} from "./../../image/image.svg"
import PropTypes from 'prop-types';


function Card ({product:{name, price, url, article, color } ,openClick, toggleFavorite, isFavorite}){
    return (
        <li className="style__li">
        <h2 className="style__title">{name}</h2>
        <h3 className="style__price">{price}</h3>
        <img className="style__img" src={url} alt="telephone"/>
        <p className="style__article">{article}</p>
        
        {/* Якщо товар обраний – зірочка має бути зафарбована будь-яким кольором. */}
        <ReactImg style={{fill: isFavorite ? "gold" : "black"}} onClick={toggleFavorite}></ReactImg> {/* іконка зірочки */}
        <span className="style__color">{color}</span>
        <Button onClick={() => openClick(article)} className="style__button" text={"Add to cart"}/>
      </li>
    )
}

Card.defaultProps = {
	onClick: () => {},
}
Card.propTypes = {
  product: PropTypes.object,
	className: PropTypes.string,
	onClick: PropTypes.func,
	text: PropTypes.string,
}
export default Card;