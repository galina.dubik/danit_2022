import { ReactComponent as Selected } from "./img/selected.svg";

function IconSelected() {
  return (
      <Selected/>
  );
}
export default IconSelected;
