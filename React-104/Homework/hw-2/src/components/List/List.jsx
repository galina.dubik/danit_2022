import React from "react";
import Card from "./../Card/Card"
import "./list.scss";
import PropTypes from "prop-types"

function List ({arrayState, openClick, toggleFavorite,  favorite}){
    return(
        <ul className="list">
          {arrayState.map((product) => ( <Card key={product.article} product={product} openClick={openClick} toggleFavorite={()=>toggleFavorite(product.article)}
          isFavorite = { favorite.includes(product.article) }/>
        ))}
      </ul>
    )
}
List.defaultProps = {
	click: () => {},
}
List.propTypes = {
	className: PropTypes.string,
	openClick: PropTypes.func,
	toggleFavorite: PropTypes.func,
}
export default List;