import React from "react";
import "./button.scss"
import PropTypes from "prop-types"

function Button ({bcgColor, text, type, onClick}){
    return (
        <button className="btn" type={type} style={{ backgroundColor: bcgColor }} onClick={onClick}>
        {text}
        </button>
    )
}
Button.defaultProps = {
	type: 'button',
	click: () => {},
}
Button.propTypes = {
    bcgColor: PropTypes.string,
	type: PropTypes.string,
	className: PropTypes.string,
	onClick: PropTypes.func,
	text: PropTypes.string,
}

export default Button;