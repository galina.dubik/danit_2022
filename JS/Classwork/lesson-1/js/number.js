const a = 5;
const PI = 3.1415;

console.log(a);
console.log(PI);

const sum = a + PI;

console.log(sum);

const minus = PI - a;

console.log(minus);

const multiply = a * PI;

console.log(multiply);

console.log(10 / 3);
console.log(10 % 3);

console.log(5 ** 2);

console.log(Math.pow(10, 2));
console.log(Math.sqrt(25));
console.log(Math.PI);
console.log(Math.random());
