// .split(чим розділено) - повертає масив

let number = prompt("Please enter your number");

while (number === null || number.trim() === "" || isNaN(+number)) {
    number = prompt("Please enter your number");
}

let result = 0;

for (let i = 0; i < number.length; i++) {
    // console.log(number[i]);
    result += +number[i]
}
console.log(result);