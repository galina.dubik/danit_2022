// * Задача 1.
// *
// * Дан массив с днями недели.
// *
// * Вывести в консоль все дни недели с помощью:
// * - Классического цикла for;
// * - Цикла for...of;
// * - Специализированного метода для перебора элементов массива forEach.
// */

// /* Дано */
// const days = [
//  'Monday',
//  'Tuesday',
//  'Wednesday',
//  'Thursday',
//  'Friday',
//  'Saturday',
//  'Sunday',
// ];

const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
    ];

    for (let i = 0; i < days.length; i++) {
        console.log(days[i]);
    }

    for (const day of days) {
        console.log(day);
    }

    let x = days.forEach(element => {
        console.log(element);
    });
    console.log(x);