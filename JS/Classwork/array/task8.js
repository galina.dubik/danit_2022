// Создать массив чисел от 1 до 100.
// Отфильтровать его таким образом, чтобы в новый массив не попали числа меньше 10 и больше 50.
// Вывести в консоль новый массив.
let arr = [];

for (let i = 1; i <= 100; i++) {
    arr.push(i);
    
}
const newArr = arr.filter((element) => element > 10 && element < 50)
console.log({newArr, arr});