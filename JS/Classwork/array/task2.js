function sumInput() {
    const array  = [];
    let sum = 0;
    let userNumber = prompt("Please enter your number");
    
    while (!((userNumber === "") || isNaN(+userNumber) || userNumber === null)){
        array.push(+userNumber);
        userNumber = prompt("Please enter your number");
    }
    
    for (let i = 0; i < array.length; i++) {
        sum += array[i];
    }
    return sum;
}
console.log(sumInput()); 