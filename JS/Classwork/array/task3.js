
let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];
function unique(arr) {
    
    let names = [];
    for (const name of arr) {
        if (names.includes(name) === false) {
            names.push(name)
        }
    }
    return names;
}
console.log(unique(values)); 