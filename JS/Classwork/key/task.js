/**
 * Задание 1.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 */



const h1 = document.createElement("h1");
h1.innerText = 'Натисніть клавішу';
document.body.append(h1);
window.addEventListener("keydown", function(event){
	h1.innerText = `Натиснута клавіша: ${event.key}` ;
})