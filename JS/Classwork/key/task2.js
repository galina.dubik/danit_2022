/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
*/





const divBackspace = document.createElement("div");
const divEnter = document.createElement("div");
const divSpace = document.createElement("div");

divBackspace.innerText = "Натиснуто Backspace: 0 разів";
divEnter.innerText = "Натиснуто Enter: 0 разів";
divSpace.innerText = "Натиснуто Space: 0 разів";

document.body.append(divBackspace, divEnter, divSpace);

let backspace = 0;
let enter = 0;
let space = 0;

window.addEventListener("keydown", (event) => {
  switch (event.code) {
    case "Backspace":
      return (divBackspace.innerText = textValue(event.code, ++backspace));
    case "Enter":
      return (divEnter.innerText = textValue(event.code, ++enter));
    case "Space":
      return (divSpace.innerText = textValue(event.code, ++space));
  }
});

function textValue(eventCode, count) {
  return `Натиснуто ${eventCode}: ${count} разів`;
}