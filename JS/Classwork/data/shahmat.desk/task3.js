/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */

const LIGHT_CELL = "#ffffff";
const DARK_CELL = "#161619";
const V_CELLS = 8;
const H_CELLS = 8;

let board = document.createElement("div");
board.classList.add("board");

for (let i = 1; i <= V_CELLS; i++) {
  let evenRow = i % 2 === 0; //по вертикали
  for (let j = 1; j <= H_CELLS; j++) {
    let evenCol = j % 2 === 0; //по горизонтали
    let cell = document.createElement("div");
    cell.classList.add("cell");
    if (evenRow) {
      if (evenCol) {
        cell.style.backgroundColor = LIGHT_CELL;
      } else {
        cell.style.backgroundColor = DARK_CELL;
      }
    } else {
      if (evenCol) {
        cell.style.backgroundColor = DARK_CELL;
      } else {
        cell.style.backgroundColor = LIGHT_CELL;
      }
    }
    board.append(cell);
  }
}
document.body.append(board);
