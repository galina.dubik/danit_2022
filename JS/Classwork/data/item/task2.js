/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */




let classRemove = document.getElementsByClassName('remove');

classRemove[0].remove();

console.log(classRemove);

let element = document.getElementsByClassName("bigger");

let elemBigger = element[0];

elemBigger.classList.replace("bigger","active");

// elemBigger.classList.remove("bigger");
// elemBigger.classList.add("active");

