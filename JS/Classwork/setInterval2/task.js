// Создать таймер. По клику на кнопку начинать отсет.
// По клику на еще одну кнопку - обнулять. Таймер вывести на страницу


const startButton = document.getElementById("begin");
const finishButton = document.getElementById("finish");
const display = document.getElementById("display");
const INIT_COUNT = 15;
let timer = null;
display.innerText = INIT_COUNT;
function startCount() {
  let initCount = INIT_COUNT;
  display.innerText = initCount;
  timer = setInterval(()=>{
    initCount--;
    if (initCount === 0) {
      display.innerText = INIT_COUNT;
      clearInterval(timer);
    }
    display.innerText = initCount;
  }, 1000);
}
function clear() {
  clearInterval(timer);
  display.innerText = INIT_COUNT;
}
startButton.addEventListener("click", startCount);
finishButton.addEventListener("click", clear);