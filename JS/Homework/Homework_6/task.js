
function createNewUser() {
  const firstName = prompt("Please, enter your first name");
  const lastName = prompt("Please, enter your last name");
  const birthday = prompt("Please enter the date of your birthday", "dd.mm.yy");
  let birthdayDate = new Date(birthday);
  let nowDate = new Date();
  // let birthdayDate = birthday.slice(3, 5) - newDate.slice(3, 5);
  // console.log(birthdayDate);

  let newUser = {
    firstName,
    lastName,
    getLogin() {
      smallName = this.firstName[0] + this.lastName;
      return smallName.toLocaleLowerCase();
      // return (this.firstName[0] + this.lastName).toLocaleLowerCase();
    },
    setFirstName() {
      newUser.defineProperty(newUser, "firstName", {
        writable: true,
        enumerable: true,
      });
    },
    setLastName() {
      newUser.defineProperty(newUser, "lastName", {
        writable: true,
        enumerable: true,
      });
    },
    getAge() {
      // return newDate.getFullYear() - birthdayDate.getFullYear();
      return Math.floor(
        (Date.parse(nowDate) - Date.parse(birthdayDate)) /
          (1000 /
          60 /
          60 /
          24 /
          365)
      );

    },
    getPassword() {
      return (
        this.firstName[0].toLocaleUpperCase() +
        this.lastName.toLocaleLowerCase() +
        birthdayDate.getFullYear()
      );
    },
  };
  return newUser;
}
// console.log(createNewUser());

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


