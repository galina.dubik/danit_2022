function createNewUser() {
    const firstName = prompt("Please, enter your first name");
    const lastName = prompt("Please, enter your last name");
    
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            smallName = this.firstName[0] + this.lastName
            return smallName.toLocaleLowerCase()
        },
        setFirstName(){
            newUser.defineProperty(newUser, "firstName", {
                writable: true,
                enumerable: true
            })
        },
        setLastName(){
            newUser.defineProperty(newUser, "lastName", {
                writable: true,
                enumerable: true
            })
        }
    }
    // console.log(newUser.getLogin());
    return newUser
};
// console.log(createNewUser());

let user = createNewUser();
console.log(user.getLogin());

