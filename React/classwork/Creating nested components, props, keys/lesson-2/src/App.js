// **Задание:**

// С помощью `create-react-app` создать новое приложение, которое будет генерировать случайные числа - лототрон лотереи.

// В приложении должно быть три компонента - `App`, `Button`, `Numbers`.

// Внутри компонента App должны быть показаны:
//  - Кнопка "Generate", которая будет генерировать случайное число от 1 до 36 и добавлять его в список доступных чисел
//  - Компонент `Numbers`, который будет показывать все числа в списке, и кнопку удаления рядом с каждым числом
 
// Обе кнопки ("Generate" и "X" внутри компонента `Numbers`) должны быть реализованы с помощью компонента `Button`.



import React from 'react';
import './App.css';
import Button from './components/Button';
import Numbers from './components/Numbers';

class App extends React.Component {
  state = {
    numbers : []
  }
  generateNumber = () => {
    const { numbers } = this.state;
    const number = Math.ceil(Math.random() * 36);
    
    this.setState({ numbers: [...numbers, number] });
  }
  render(){
    return (
      <div className="App">
        <Numbers list = {this.state.numbers} ></Numbers>
        <Button onClick={this.generateNumber}>
        Generate
        </Button>
      </div>
    );
  }
}

export default App;
