import Button from "./Button";

function Numbers({ list }) {
  return (
    <ul>
      {list.map((elem, index) => (
        <li key={`${index}:${elem}`}>
          {elem}
          <Button>X</Button>
        </li>
      ))}
    </ul>
  );
}
export default Numbers;
