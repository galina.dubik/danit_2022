// Задание:

// С помощью `create-react-app` создать новое приложение, которое будет выводить на экран простой счетчик.

// Он должен показывать на экране число "50%", справа и слева от которого будут кнопки "-" и "+".

// При нажатии на кнопку "-", значение числа должно уменьшаться на 10%. Значение не может быть меньше чем 0. Если значение уже равно 0, при нажатии на кнопку "-" ничего не происходит.

// При нажатии на кнопку "+", значение числа должно увеличиваться на 10%. Значение не может быть больше чем 100.



import React from "react";
// import logo from "./logo.svg";
import "./App.css";


class App extends React.Component {
    state = {
        volume: 50
    };
    minusVolume = () => {
        // if (this.state.volume > 0) {
        //     this.setState({volume: this.state.volume - 10});
        // }
        this.setState((prevState) => {
            if (prevState.volume > 0) {
               return {volume: prevState.volume - 10}
            } else {
                return {volume: prevState.volume}
            }
        })
    }
    plusVolume = () => {
        // if (this.state.volume <100) {
        //     this.setState({volume: this.state.volume + 10});
        // }
        this.setState((prevState) => {
            if (prevState.volume <100) {
                return {volume: prevState.volume + 10}
            } else {
                return {volume: prevState.volume}
            }
        })
    }

    render() {

        return (
            <div className="App">
                <button onClick={this.minusVolume}>-</button>
                <span>{this.state.volume}%</span>
                <button onClick={this.plusVolume}>+</button>
            </div>
        );
    }
}

export default App;

