import React from "react";
import Button from "../Button";
import "./cart.scss"

class Cart extends React.Component {
  render() {
    const { name, price, url, article, color } = this.props.product;
    return (
      <li className="style__li">
        <h2 className="style__title">{name}</h2>
        <h3 className="style__price">{price}</h3>
        <img className="style__img" src={url} alt="telephone"/>
        <p className="style__article">{article}</p>
        <span className="style__color">{color}</span>
        <Button className="style__button">Add to cart</Button>
      </li>
    );
  }
}
export default Cart;
