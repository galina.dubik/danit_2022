import React from "react";
import Cart from "../Cart/Cart";
import './list.scss'

class List extends React.Component {
  render() {
    const { arrayState } = this.props;
    return (
      <ul className="list">
          {arrayState.map((product) => ( <Cart key={product.article} product={product} />
        ))}
      </ul>
    );
  }
}
export default List;
