// Створити новий React додаток з двома модальними вікнами.

// Технічні вимоги:
// Створити програму за допомогою create-react-app.
// Створити на головній сторінці 2 кнопки з текстом Open first modal та Open second modal.
// По кліку на кожну з кнопок має відкриватись відповідне модальне вікно.
// Створити компонент Button який повинен мати такі властивості, що передаються з батьківського компонента:
// Колір фону (властивість backgroundColor)
// Текст (властивість text)
// Функція при натисканні (властивість onClick)
// Створити компонент Modal який повинен мати такі властивості, що передаються з батьківського компонента:
// Текст заголовка модального вікна (властивість header)
// Чи повинен бути хрестик для закриття вікна праворуч вгорі (boolean властивість closeButton, значення true/false)
// Основний текст модального вікна, який буде показаний у його центральній частині (властивість text)
// Кнопки, що знаходяться у нижній частині модального вікна, передані у вигляді коду у форматі JSX (властивість actions)
// При відкритому модальному вікні частина сторінки, що залишилася, повинна бути затемнена за допомогою темного напівпрозорого фону.
// Модальне вікно має закриватися при натисканні області зовні його контенту.
// Стилізувати кнопки та модальні вікна за допомогою SCSS
// Кнопки мають бути різних кольорів
// Модальні вікна мають містити різний текст.
// Дизайн модального вікна дається в PSD файлі.
// Одне модальне вікно зробити як у дизайні. Для другого потрібно використовувати інший текст та інші кнопки (виберіть будь-які).
// Усі компоненти мають бути створені у вигляді ES6 класів.

import React from "react";
import "./App.css";
import Button from "./components/Button";
import Modal from "./Modal/modal";

class App extends React.Component {
  state = {
    open: null,
  };

  // const {modalActive, setModalActive} = useState(false)
  // btnProps = {
  //   backgroundColor: "blue",
  //   text: "Do you want to delete this file?",
  //   onSubmit: ()=>null,
  //   onClose: ()=>null
  // }

  openClick = (modalId) => {
    this.setState({ open: modalId });
    // console.log("open");
  };
  closeClick = () => {
    this.setState({ open: null });
    // console.log("close");
  };
  render() {
    return (
      <div className="App">
        <Button onClick={() => this.openClick("modalId1")} bgColor={"blue"}>
          Open second modal
        </Button>
        <Button onClick={() => this.openClick("modalId2")} bgColor={"yellow"}>
          Open first modal
        </Button>
        {this.state.open === "modalId1" && (
          <Modal
            actions={
              <>
                <button onClick={this.closeClick} className="modal__button--ok">
                  Ok
                </button>{" "}
                <button
                  onClick={this.openClick}
                  className="modal__button--cancel"
                >
                  Cancel
                </button>
              </>
            }
            className="modal1"
            closeButton={true}
            onClick={this.closeClick}
            header="Do you want to delete this file?"
            text="Onse you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          ></Modal>
        )}
        {this.state.open === "modalId2" && (
          <Modal
            actions={
              <>
                <button onClick={this.closeClick} className="modal__button--ok">
                  Ok
                </button>{" "}
                <button
                  onClick={this.openClick}
                  className="modal__button--cancel"
                >
                  Cancel
                </button>
              </>
            }
            className="modal2"
            closeButton={true}
            onClick={this.closeClick}
            header="Do you want to delete this file?"
            text="Are you sure you want to delete it?"
          ></Modal>
        )}
      </div>
    );
  }
}
export default App;


