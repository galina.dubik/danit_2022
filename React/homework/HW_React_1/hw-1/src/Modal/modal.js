import "./modal.scss";
import React from "react";
import { ReactComponent as CloseIcon } from "./img/closeIcon.svg";

class Modal extends React.Component {
  render() {
    const { header, text, closeButton, onClick, actions, className } =
      this.props;
    // console.log(props);
    return (
      <div className="modal__background" onClick={onClick}>
        <div className={`modal ${className}`} onClick={elem => elem.stopPropagation()}>
          <div className="modal__header">
            <h2 className="modal__title">{header}</h2>
            {closeButton && <CloseIcon onClick={onClick} />}
          </div>
          <div className="modal__content">
            <p className="content-text">{text}</p>
          </div>
          <div className="modal__button">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
