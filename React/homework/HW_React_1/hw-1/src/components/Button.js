function Button({ bgColor, onClick, children }) {
  return (
    <button style={{ backgroundColor: bgColor }} onClick={onClick}>
      {children}
    </button>
  );
}

export default Button;
